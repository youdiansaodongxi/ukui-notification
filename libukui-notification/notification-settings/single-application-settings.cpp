/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 */

#include "single-application-settings.h"
#include <QJsonObject>
#include <QDebug>
#include "settings-properties-info.h"
#include "applications-settings.h"


namespace UkuiNotification{
class SingleApplicationSettingsPrivate
{
    friend class SingleApplicationSettings;
public:
    SingleApplicationSettingsPrivate();
    SingleApplicationSettingsPrivate(const SingleApplicationSettingsPrivate &other);
    ~SingleApplicationSettingsPrivate();
private:
    QJsonObject m_settings;
    QString m_desktopEntry;
};
}
using namespace UkuiNotification;

SingleApplicationSettingsPrivate::SingleApplicationSettingsPrivate()
{
}

SingleApplicationSettingsPrivate::SingleApplicationSettingsPrivate(const SingleApplicationSettingsPrivate &other)
{
    m_settings = other.m_settings;
    m_desktopEntry = other.m_desktopEntry;
}

SingleApplicationSettingsPrivate::~SingleApplicationSettingsPrivate()
{

}

SingleApplicationSettings::SingleApplicationSettings(const QString &desktopEntry, QObject *parent)
        : QObject(parent)
        , d(new SingleApplicationSettingsPrivate)
{
    d->m_desktopEntry = desktopEntry;
    init();
}

SingleApplicationSettings::SingleApplicationSettings(const SingleApplicationSettings &other)
        : d(new SingleApplicationSettingsPrivate(*other.d))
{
    init();
}

SingleApplicationSettings &SingleApplicationSettings::operator=(const SingleApplicationSettings &other)
{
    *d = *other.d;
    init();
    return *this;
}
SingleApplicationSettings &SingleApplicationSettings::operator=(SingleApplicationSettings &&other) Q_DECL_NOEXCEPT
{
    d = other.d;
    other.d = nullptr;
    init();
    return *this;
}

SingleApplicationSettings::~SingleApplicationSettings()
{
    if(d) {
        delete d;
        d = nullptr;
    }
}

void SingleApplicationSettings::init()
{
    d->m_settings = ApplicationsSettings::self()->getAppSettings(d->m_desktopEntry);
    if(d->m_desktopEntry != QStringLiteral("default") && !d->m_desktopEntry.isEmpty()) {
        connect(ApplicationsSettings::self(), &ApplicationsSettings::applicationUninstalled, this, &SingleApplicationSettings::appUninstalled);
        connect(ApplicationsSettings::self(), &ApplicationsSettings::settingsUpdated, this, &SingleApplicationSettings::settingsDataChanged);
    }
}

bool SingleApplicationSettings::allowNotify() const
{
    return d->m_settings.value(SettingsPropertiesInfo(SettingsProperty::AllowNotify).name()).toVariant().toBool();
}

bool SingleApplicationSettings::allowSound() const
{
    return d->m_settings.value(SettingsPropertiesInfo(SettingsProperty::AllowSound).name()).toVariant().toBool();
}

bool SingleApplicationSettings::showContentOnLockScreen() const
{
    return d->m_settings.value(SettingsPropertiesInfo(SettingsProperty::ShowContentOnLockScreen).name()).toVariant().toBool();
}

bool SingleApplicationSettings::showNotificationOnLockScreen() const
{
    return d->m_settings.value(SettingsPropertiesInfo(SettingsProperty::ShowNotificationOnLockScreen).name()).toVariant().toBool();
}

//SettingsProperty::Property UkuiNotification::SingleApplicationSettings::popupStyle() const
//{
//    return SettingsPropertiesInfo::fromName(d->m_settings.value(SettingsPropertiesInfo(SettingsProperty::PopupStyle).name()).toString()).property();
//}

void SingleApplicationSettings::settingsDataChanged()
{
    QJsonObject data = ApplicationsSettings::self()->getAppSettings(d->m_desktopEntry);
    if(data.isEmpty()) {
        return;
    }
    for(SettingsProperty::Property property : SINGLE_APPLICATION_SETTINGS) {
        QString name = SettingsPropertiesInfo(property).name();
        QJsonValue value = data.value(name);
        if(d->m_settings.value(name) != value) {
            d->m_settings.insert(name, value);
            switch (property) {
                case SettingsProperty::AllowNotify:
                    Q_EMIT allowNotifyChanged(value.toVariant().toBool());
                    break;
                case SettingsProperty::AllowSound:
                    Q_EMIT allowSoundChanged(value.toVariant().toBool());
                    break;
                case SettingsProperty::ShowContentOnLockScreen:
                    Q_EMIT showContentOnLockScreenChanged(value.toVariant().toBool());
                    break;
                case SettingsProperty::ShowNotificationOnLockScreen:
                    Q_EMIT showNotificationOnLockScreenChanged(value.toVariant().toBool());
                    break;
//                case SettingsProperty::PopupStyle:
//                    Q_EMIT popupStyleChanged(SettingsPropertiesInfo::fromName(value.toString()).property());
//                    break;
                default:
                    break;
            }
        }
    }
}

void SingleApplicationSettings::appUninstalled(const QString &desktopEntry)
{
    if(d->m_desktopEntry == desktopEntry) {
        Q_EMIT uninstalled();
    }
}

void SingleApplicationSettings::setAllowNotify(bool enable)
{
    qWarning() << d->m_desktopEntry << enable;
    ApplicationsSettings::self()->setAppSetting(d->m_desktopEntry, SettingsProperty::AllowNotify, enable);
}

void SingleApplicationSettings::setAllowSound(bool enable)
{
    ApplicationsSettings::self()->setAppSetting(d->m_desktopEntry, SettingsProperty::AllowSound, enable);
}

void SingleApplicationSettings::setShowContentOnLockScreen(bool enable)
{
    ApplicationsSettings::self()->setAppSetting(d->m_desktopEntry, SettingsProperty::ShowContentOnLockScreen, enable);
}

void SingleApplicationSettings::setShowNotificationOnLockScreen(bool enable)
{
    ApplicationsSettings::self()->setAppSetting(d->m_desktopEntry, SettingsProperty::ShowNotificationOnLockScreen, enable);
}

//void SingleApplicationSettings::setPopupStyle(SettingsProperty::Property style)
//{
//    ApplicationsSettings::self()->setAppSetting(d->m_desktopEntry, SettingsProperty::PopupStyle, style);
//}

QString SingleApplicationSettings::desktopEntry() const
{
    return d->m_desktopEntry;
}
