/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 */

#include "applications-settings.h"
#include <mutex>
#include <QQmlEngine>
#include <QDebug>
#include "settings-manager.h"
#include <QJsonObject>

using namespace UkuiNotification;
static std::once_flag flag;
static ApplicationsSettings *s_self;
namespace UkuiNotification{
class ApplicationsSettingsPrivate
{
    friend class ApplicationsSettings;
private:
    ApplicationsSettingsMap m_cache;
    QJsonObject m_settingsData;
};
}

ApplicationsSettings *UkuiNotification::ApplicationsSettings::self()
{
    std::call_once(flag, [ & ] {
        s_self = new ApplicationsSettings();
    });
    return s_self;
}

ApplicationsSettings::ApplicationsSettings(QObject *parent) : QObject(parent), d(new ApplicationsSettingsPrivate)
{
    d->m_settingsData = SettingsManager::self()->getAllAppSettings();
    connect(SettingsManager::self(), &SettingsManager::appUninstalled, this, &ApplicationsSettings::onApplicationUninstalled);
    connect(SettingsManager::self(), &SettingsManager::settingsDataChanged, this, &ApplicationsSettings::settingsDataChanged);
    qmlRegisterUncreatableType<UkuiNotification::SingleApplicationSettings>("org.ukui.notification.client", 1, 0, "SingleApplicationSettings", "");
}

ApplicationsSettings::~ApplicationsSettings()
{
    if(d) {
        delete d;
        d = nullptr;
    }
}

SingleApplicationSettings *ApplicationsSettings::creatSettings(const PopupNotification &notification)
{
    return creatSettings(notification.desktopEntry());
}

ApplicationsSettingsMap &ApplicationsSettings::getAllApplicationsSettings()
{
    for(const QString &desktopEntry : d->m_settingsData.keys()) {
        if(!d->m_cache.contains(desktopEntry)) {
            d->m_cache.insert(desktopEntry, new SingleApplicationSettings(desktopEntry));
        }
    }
    return d->m_cache;
}

QJsonObject ApplicationsSettings::getAppSettings(const QString &appDesktopName)
{
    if(appDesktopName == QStringLiteral("default") || appDesktopName.isEmpty()) {
        return SettingsManager::self()->getAppDefaultSettings();
    }
    QJsonValue value = d->m_settingsData.value(appDesktopName);
    if(value.isUndefined()) {
        return SettingsManager::self()->getAppDefaultSettings();;
    }
    return value.toObject();
}

bool ApplicationsSettings::setAppSetting(const QString &appDesktopName, SettingsProperty::Property key,
                                         const QVariant &value)
{
    return SettingsManager::self()->setAppSettings(appDesktopName, key, value);
}

void ApplicationsSettings::clearCache()
{
    ApplicationsSettingsMap::const_iterator i = d->m_cache.constBegin();
    while(i != d->m_cache.constEnd()) {
        ++i;
        delete i.value();
    }
    d->m_cache.clear();
}

void ApplicationsSettings::settingsDataChanged()
{
    QJsonObject data = SettingsManager::self()->getAllAppSettings();
    if(d->m_settingsData == data) {
        return;
    }
    for(const QString &desktopEntry : data.keys()) {
        bool newInstalled = !d->m_settingsData.contains(desktopEntry);
        d->m_settingsData.insert(desktopEntry, data.value(desktopEntry));
        if(newInstalled) {
            Q_EMIT applicationInstalled(desktopEntry);
        }
    }
    Q_EMIT settingsUpdated();
}

void ApplicationsSettings::onApplicationUninstalled(const QString &desktopEntry)
{
    if (d->m_settingsData.contains(desktopEntry)) {
        d->m_settingsData.remove(desktopEntry);
    }
    Q_EMIT applicationUninstalled(desktopEntry);
}

SingleApplicationSettings *ApplicationsSettings::creatSettings(QString desktopEntry)
{
    if(desktopEntry.isEmpty()) {
        desktopEntry = QStringLiteral("default");
    }
    if(d->m_cache.contains(desktopEntry)) {
        return d->m_cache[desktopEntry];
    } else {
        return d->m_cache.insert(desktopEntry, new SingleApplicationSettings(desktopEntry)).value();
    }
}
