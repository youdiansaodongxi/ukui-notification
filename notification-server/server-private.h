/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 */

#ifndef UKUI_NOTIFICATION_SERVER_PRIVATE_H
#define UKUI_NOTIFICATION_SERVER_PRIVATE_H
#include <QObject>
#include <QDBusContext>
#include <QMap>
class QDBusServiceWatcher;
namespace NotificationServer {
class Notification {
    friend class ServerPrivate;
    QString m_appName;
    uint m_id = 0;
    QString m_appIcon;
    QString m_summary;
    QString m_body;
    QStringList m_actions;
    QVariantMap m_hints;
    int m_timeout;
};
class ServerPrivate : public QObject, protected QDBusContext
{
    Q_OBJECT
//dbus interface
public:
    QStringList GetCapabilities() const;
    uint Notify(const QString &app_name,
                uint replaces_id,
                const QString &app_icon,
                const QString &summary,
                const QString &body,
                const QStringList &actions,
                const QVariantMap &hints,
                int timeout);
    void CloseNotification(uint id);
    QString GetServerInformation(QString &vendor, QString &version, QString &spec_version) const;

    /**
     * 注册成为通知中心客户端
     */
    void RegisterClient();
    /**
     * 注销
     */
    void UnRegisterClient();
    /**
     * 通知中心关闭通知
     * @param id 通知id
     * @param reason 关闭原因
     */
    void CloseNotification(uint id, uint);
    void InvokeAction(uint id, const QString &action_key);
    void UpdateUnreadMessagesNumber(const QString &desktopEntry, uint number);
Q_SIGNALS:
    void NotificationClosed(uint id, uint reason);
    void ActionInvoked(uint id, const QString &actionKey);
    void ActivationToken(uint id, const QString &ActivationToken);
    void UnreadMessagesNumberUpdated(const QString &desktopEntry, uint number);
public:
    explicit ServerPrivate(QObject *parent = nullptr);
    ~ServerPrivate() override;
    bool init();

    static QString notificationServiceName();
    static QString notificationServicePath();
    static QString notificationServiceInterface();
private:
    void sendCache();
    QDBusServiceWatcher *m_notificationWatchers = nullptr;
    uint m_increasedNotificationId = 1;
    QMap<uint, Notification> m_notificationsCache;
};



} // NotificationServer

#endif //UKUI_NOTIFICATION_SERVER_PRIVATE_H
