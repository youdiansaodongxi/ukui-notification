<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN" sourcelanguage="en">
<context>
    <name>Notice</name>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="52"/>
        <source>Notice</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
    <message>
        <source>NotFaze Mode</source>
        <translation type="vanished">མི་དམངས་ཀྱི་དཔེ་དབྱིབས་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="149"/>
        <source>Do not disturb mode</source>
        <translation>མི་དམངས་ཀྱི་དཔེ་དབྱིབས་མིན་པ།</translation>
        <extra-contents_path>/Notice/Do not disturb mode Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="151"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation>(བརྡ་ཐོ་གཏོང་བའི་འཕྲེད་འགེལ་སྦྱར་ཡིག་དང་། བརྡ་གཏོང་ཡི་གེ་སྦས་སྐུང་བྱས་ནས་བརྡ་ཁྱབ་ཀྱི་སྒྲ་གྲགས་ཡོང་། )</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="226"/>
        <source>Automatically turn on</source>
        <translation>རང་འགུལ་གྱིས་ཁ་ཕྱེ་བ།</translation>
        <extra-contents_path>/Notice/Do not disturb mode Mode/Automatically turn on</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="251"/>
        <source>to</source>
        <translation>དེ་ལྟར་བྱས་ན་</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="277"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation>བརྙན་ཤེལ་མང་པོ་སྦྲེལ་མཐུད་བྱེད་སྐབས་རང་འགུལ་གྱིས་ཁ་ཕྱེ་བ།</translation>
        <extra-contents_path>/Notice/Do not disturb mode/Automatically turn on when multiple screens are connected</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="283"/>
        <source>Automatically open in full screen mode</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་པོའི་རྣམ་པའི་ཐོག་ནས་རང་འགུལ་གྱིས་སྒོ་</translation>
        <extra-contents_path>/Notice/Do not disturb mode/Automatically open in full screen mode</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="289"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation>རང་འགུལ་གྱིས་ཉེན་བརྡ་གཏོང་བའི་དྲན་སྐུལ་བྱེད་སྟངས་ལ་སུན་པོ་བཟོ་མི་རུང་།</translation>
        <extra-contents_path>/Notice/Do not disturb mode/Allow automatic alarm reminders in Do Not Disturb mode</extra-contents_path>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="318"/>
        <source>Notice Settings</source>
        <translation>བརྡ་ཐོའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../notification-ukcc-plugin.cpp" line="320"/>
        <source>Get notifications from the app</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་ཁྲོད་ནས་བརྡ་ཐོ་གཏོང་དགོས།</translation>
        <extra-contents_path>/notice/Get notifications from the app</extra-contents_path>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../notice-menu.cpp" line="104"/>
        <source>Beep sound when notified</source>
        <translation>བརྡ་ཐོ་གཏོང་སྐབས་སྐད་ཅོར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../notice-menu.cpp" line="110"/>
        <source>Show message  on screenlock</source>
        <translation>བརྙན་ཤེལ་སྟེང་ནས་ཆ་འཕྲིན་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../notice-menu.cpp" line="116"/>
        <source>Show noticfication  on screenlock</source>
        <translation>བརྙན་ཤེལ་སྟེང་ནས་དོ་སྣང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Notification Style</source>
        <translation type="vanished">བརྡ་ཐོ་གཏོང་སྟངས།</translation>
    </message>
    <message>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation type="vanished">འཕྲེད་འགེལ་ཡི་གེ། བརྙན་ཤེལ་གྱི་གཡས་ཟུར་དུ་མངོན་པ་མ་ཟད། རང་འགུལ་གྱིས་མེད་པར་གྱུར་པ།</translation>
    </message>
    <message>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation type="vanished">རྩེ་མོ། འཆར་ངོས་སུ་ཉར་ཚགས་བྱས་ནས་སྒོ་མ་བརྒྱབ་གོང་ལ་ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation type="vanished">གཅིག་ཀྱང་མ་ལུས་པར་འཆར་ངོས་སུ་བརྡ་ཐོ་མི་མངོན་པར་བརྡ་ཁྱབ་ལྟེ་གནས་སུ་འགྲོ་རྒྱུ་རེད།</translation>
    </message>
</context>
</TS>
